import java.util.Random;
import java.util.Scanner;

/**
 * Created by Tshepo on 9/20/2015.
 */
public class bullsAndCows {
    public static void main(String[] args) {
        System.out.println("The computer has generated a unique 4 digit number for the bulls and cows.\n"
                + "Try to guess the 4 digits number in 5 attempts.\n_______________________________________________________\n");
        int[] random = numberGenerator();
        int maximum = 5;
        int indexMatch = 0;
        int checkmatch = 0;
        while (maximum > 0 && indexMatch != 4) {
            int[] guess = getGuess();
            indexMatch = 0;
            checkmatch = 0;
            for (int count = 0; count < guess.length; count++) {
                if (guess[count] == random[count]) {
                    indexMatch++;
                } else if (guess[count] == random[0] || guess[count] == random[1] || guess[count] == random[2] || guess[count] == random[3]) {
                    checkmatch++;
                }
            }
            if (indexMatch == 4) {
                System.out.print("Well done! You won the game! The number is: ");
                for (int index = 0; index < guess.length; index++) {
                    System.out.print(guess[index]);
                }
            } else {
                maximum--;
                if (maximum > 1) {
                    System.out.println("You have guess " + indexMatch + " correct number in correct position," +
                            " and " + checkmatch + " correct number in incorrect position. \n" + maximum + " attempt remaining.");
                } else if (maximum == 1) {
                    System.out.println("You have guess " + indexMatch + " correct number in correct position," +
                            " and " + checkmatch + " correct number in incorrect position. \nLast attempt!. Good luck");
                } else {
                    System.out.println("Sorry, you failed to guess the number in 5 attempts.");
                    System.out.print("The generated number is: ");
                    for (int i = 0; i < random.length; i++) {
                        System.out.print(random[i]);
                    }
                }
            }
        }
    }

    public static int[] getGuess() {
        //allow the user to guess the numbers
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter your guess: ");
        String input = keyboard.nextLine();
        if (input.length() != 4 || input.replaceAll("\\D", "").length() != 4) {
            System.out.println("Invalid number. You must enter 4 digits between 0-9 only.");
            return getGuess();
        }
        int[] guess = new int[4];
        for (int value = 0; value < 4; value++) {
            guess[value] = Integer.parseInt(String.valueOf(input.charAt(value)));
        }
        return guess;
    }

    public static int[] numberGenerator() {

        Random generateNum = new Random();
        int[] generateArray = {10, 10, 10, 10};

        for (int number = 0; number < generateArray.length; number++) {
            int temp = generateNum.nextInt(9);
            while (temp == generateArray[0] || temp == generateArray[1] || temp == generateArray[2] || temp == generateArray[3]) {
                temp = generateNum.nextInt(9);
            }
            generateArray[number] = temp;
        }
        return generateArray;
    }
}

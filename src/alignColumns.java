/**
 * Created by Tshepo on 10/8/2015.
 */
public class alignColumns {
    public static void main(String [] args){
        String [] lines = {"Given$a$text$file$of$many$lines,$where$fields$within$a$line",
                "are$delineated$by$a$single$'dollar'$character,$write$a$program",
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each",
                "Further,$allow$for$each$word$in$a$column$to$be$either$left",
                "justified,$right$justified,$or$center$justified$within$its$column"};

        rightAlign(lines);
        leftAlign(lines);
        centerAlign(lines);

    }

    public static void rightAlign(String[] paragraph)
    {
        System.out.print("----------------------right align--------------------------");
        String[] info = null;

        for (int paraIndex = 0; paraIndex < paragraph.length; paraIndex++) {
            info = paragraph[paraIndex].split("\\$");
            for (String line : info) {
                System.out.printf("%15s", line);

            }
            System.out.println();

        }
    }

    public static void leftAlign(String[] paragraph)
    {
        System.out.println("----------------------Left align--------------------------");

        String[] info = null;

        for (int paraIndex = 0; paraIndex < paragraph.length; paraIndex++) {
            info = paragraph[paraIndex].split("\\$");
            for (String line : info) {
                System.out.printf("%-15s", line);
            }
            System.out.println();
        }
    }

    public static void centerAlign(String[] paragraph) {
        System.out.println("----------------------center align--------------------------");

        String[] info = null;
        int width = 20;
        for (String line : paragraph) {
            info = line.split("\\$");

            for (String row : info) {

                int sizeTable = width - row.length();
                int columnStart = row.length() + sizeTable / 2;

                row = String.format("%" + columnStart + "s", row);
                row = String.format("%-" + width + "s", row);

                System.out.print(row);
            }

            System.out.println();
        }
    }
}

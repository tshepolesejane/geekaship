import javax.swing.*;

/**
 * Created by Tshepo on 10/8/2015.
 */
public class romanNumerals {
    public static void main(String[] args) {
        String input = JOptionPane.showInputDialog(null, "Enter the roman numerals either in additive form or subtractive form") ;
        String[] romanValue = input.split(",");
        String sum = "";
        String finalValue = "";

        for (int i = 0; i < romanValue.length; i++) {
            try {
                /*Convert the numbers in string to int*/
                finalValue =romanValue[i];
            } catch (Exception error) {

            }
            sum += finalValue;
        }
        System.out.println(decode(finalValue));
    }

    private static int decodingRoman(char inputChar) {
        switch(inputChar) {
            case 'M': return 1000;
            case 'D': return 500;
            case 'C': return 100;
            case 'L': return 50;
            case 'X': return 10;
            case 'V': return 5;
            case 'I': return 1;
            default: return 0;
        }
    }

    public static int decode(String roman) {
        int result = 0;
        String uRoman = roman.toUpperCase();
        for(int i = 0;i < uRoman.length() - 1;i++) {//loop over all but the last character
            //if this character has a lower value than the next character
            if (decodingRoman(uRoman.charAt(i)) < decodingRoman(uRoman.charAt(i + 1))) {
                //subtractive form
                result -= decodingRoman(uRoman.charAt(i));
            } else {
                //additive form
                result += decodingRoman(uRoman.charAt(i));
            }
        }
        //decode the last character, which is always added
        result += decodingRoman(uRoman.charAt(uRoman.length() - 1));
        return result;
    }
}

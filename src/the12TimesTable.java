/**
 * Created by Tshepo on 10/8/2015.
 */
public class the12TimesTable {
    public static void main (String [] args){
        int [] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int multiple;

        for (int x = 0; x < nums.length; x++){

            System.out.println("\n" + "- Table of: " +nums[x] + " -" + "\n");

            for(int i = 0; i < nums.length; i++ ){
                int num = i + 1;
                multiple = nums[x] * num;
                System.out.println( nums[x] + " X " + num + " = " + multiple);
            }

        }
    }
}

/**
 * Created by Tshepo on 10/8/2015.
 */
public class callingMethods {
    //Calling private method
    private int num1;
    private int num2;
    private String name;
    private static int prints = 4;

    public static void main(String args[]) {


        callingMethods methods = new callingMethods();

        int results = methods.totalReturn(6, 8, 17);

        int value=methods.totalReturn(56);
        int value1 = methods.totalReturn(67);
        System.out.println(results);
        System.out.println(value1);
        System.out.println(value);
        methods.PrintOutputs();
        methods.myNameRef();
        methods.mySurnameValue();

    }


    callingMethods() {

    }

    public String myNameRef() {
        String name = "Tshepo";
        System.out.println("Name is : " + name);
        return name;
    }

    public void mySurnameValue(){
        String surname = "Lesejane";
        System.out.println("Surname is : " + surname);
    }


     //Calling a protected method

    protected int totalReturn(int num1, int num2, int sum)
    {
        this.num1 = num1;
        this.num2 = num2;

        sum = num1 + num2;

        return sum;
    }

    protected int totalReturn(int number2) {
        return this.num2;
    }
    //calling public methods

    public void PrintOutputs() {

        System.out.println(prints);
    }

}

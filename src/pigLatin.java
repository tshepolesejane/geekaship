import javax.swing.*;

/**
 * Created by Tshepo on 10/8/2015.
 */
public class pigLatin {
    public static void main ( String [] args){
        String input = JOptionPane.showInputDialog(null, "Enter any word you want to be translated");
        int count = 0;
        int inputIndex = 0;

        String suffixSyllable = "way";
        String prefixSyllable = "ay";
        String translated;

        while (vowelCheck(input.charAt(inputIndex)) == false){

            if(vowelCheck(input.charAt(inputIndex)) == false){
                count += 1;
            }

            inputIndex += inputIndex;
        }
        if (count == 0){
            translated =  input + "-" + suffixSyllable ;
        }else {
            String swapped = input.substring(count);
            String tempString = "";
            tempString = tempString + swapped;

            translated = prefixSyllable + "-" + tempString;
        }

        System.out.println(translated);

    }

    public static boolean vowelCheck(char userInput){
        boolean charIsVowel = false;
        char [] vowels = {'a', 'e', 'i', 'o', 'u'};
        int compareVowel = 0;

        if ((userInput == vowels[0])) {

            charIsVowel = true;

        } else if ((userInput == vowels[1])) {

            charIsVowel = true;

        }else if ((userInput == vowels[2])){

            charIsVowel = true;

        }else if ((userInput == vowels[3])){

            charIsVowel = true;

        }else if ((userInput == vowels[0])){
            charIsVowel = true;
        }

        return charIsVowel;
    }
}

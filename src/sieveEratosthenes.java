import java.util.Scanner;

/**
 * Created by Tshepo on 10/9/2015.
 */
public class sieveEratosthenes {
    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        System.out.println("Enter yur maximum number: ");
        int num = scan.nextInt();

        int[] numbers = new int[num + 1];

        int count = 2;
        for (int numIndex = 2; numIndex <= num; numIndex++) {
            numbers[numIndex] = count;
            count++;
        }

        for(int numIndex = 2; (numIndex * numIndex) <= num; numIndex++ ){
            for (int j = (numIndex * numIndex); j <= num; j = j + numIndex) {
                numbers[j] = 0;
            }
        }
        for (int numIndex = 2; numIndex <= num; numIndex++) {
            if (numbers[numIndex] != 0) {
                System.out.println("Prime Numbers: " + numbers[numIndex]);
            }
        }
    }
}

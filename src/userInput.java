import java.util.Scanner;

/**
 * Created by Tshepo on 10/8/2015.
 */
public class userInput {
    public static void main ( String [] args){
        int userNum;

        Scanner scan = new Scanner(System.in);
        String userInput = scan.next();
        userNum = Integer.parseInt(userInput);

        System.out.println("The user input as a String "+ userInput);
        System.out.println("The user input as an integer"+ userNum);
    }
}

/**
 * Created by Tshepo on 10/8/2015.
 */
public class rgbNumbers {
    public static void main (String [] args){
        int R,G,B;
        R  = 128;
        G = 192;
        B = 79;

        String hexOutput = String.format("#%02x%02x%02x",R,G,B);

        System.out.println("RGB numbers in hexadecimal format :" + hexOutput);
    }
}

import java.util.Scanner;

/**
 * Created by Tshepo on 10/7/2015.
 */
public class morseCode {
    public  static void main (String [] args){
        char [] normalChar = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','Q','R','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','0'};
        String [] morseChar ={".-","-..","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-...","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--..",".----","..---","...--","....-",".....","-....","--...","---..","---.","-----"};

        Scanner scanInput = new Scanner(System.in);
        String userInput = scanInput.nextLine();
        userInput.toUpperCase();

        translateInput(userInput, normalChar, morseChar);
    }
    public static void translateInput(String input, char [] englishChar, String[] morseCodeChar){
        String translatedText = "";

            for (int check = 0; check < input.length(); check++) {
                char compare = input.charAt(check);
                if(Character.isLetter(compare) || Character.isDigit(compare)) {
                    for (int index = 0; index < englishChar.length; index++) {
                        if (compare == englishChar[index]) {
                            translatedText = translatedText + " " + morseCodeChar[index];
                            System.out.print(morseCodeChar[index]);
                            System.out.println("--------------------------------");
                        }
                    }
                }
            }
        System.out.println("Translated input to morseCode " + translatedText);
        String [] tempMorse = input.split(" ");

            for (int tempMorseIndex = 0; tempMorseIndex < tempMorse.length; tempMorseIndex++){
                for(int morseCharIndex = 0; morseCharIndex < morseCodeChar.length; morseCharIndex++ ){
                    if(tempMorse[tempMorseIndex] == morseCodeChar[morseCharIndex]){
                        translatedText = translatedText + " " + englishChar[morseCharIndex];
                        System.out.print(englishChar[morseCharIndex]);
                    }
                }
            }
        System.out.println("Translated text to english " + translatedText);
        }
    }


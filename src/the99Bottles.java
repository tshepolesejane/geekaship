/**
 * Created by Tshepo on 10/2/2015.
 */
public class the99Bottles {
    public static void main ( String [] args) {
        String line1, line2, line3;

        line1 = "bottles of beer on the wall ";
        line2 = " bottles of beer";
        line3 = "Take one down";

        int bottles = 99;

        while (bottles >= 0) {

            if (bottles > 1) {
                System.out.println(bottles + " " + line1);
            } else if (bottles == 1) {
                System.out.println(bottles + " bottle of beer on the wall " + "\n" + line2);
            }else if (bottles > 0) {
                System.out.println(bottles + " " + line2 + "\n" + line3);
            }else{
                System.out.print(bottles);
            }
            bottles -= 1;
        }
    }
}

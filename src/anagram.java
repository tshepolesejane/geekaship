import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tshepo on 10/2/2015.
 */
public class anagram {
    public static void main(String [] args){
        anagramFunction anagramChild = new anagramFunction();
        ArrayList<String> wordsToPrint= new ArrayList<String>();
        try{
            String[] wordFromTheFile = anagramChild.getWordsFromFile();
            Scanner scan = new Scanner(System.in);

            String input = scan.nextLine();

            wordsToPrint = anagramChild.getAnagram(input,wordFromTheFile);
            ArrayList<String> theAnagram = loadAnagram(input,wordsToPrint);


            for (int i = 0; i <wordsToPrint.size() ; i++) {
                System.out.print(theAnagram.get(i)+"\t");
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    public  static ArrayList<String> loadAnagram(String input, ArrayList<String> possibleWords){

        ArrayList<String> ArrayListAnagram = new ArrayList<String>();
        anagramFunction anagram = new anagramFunction();

        for (int i = 0; i <possibleWords.size() ; i++) {

            if(anagram.isAnAnagram(input, possibleWords.get(i))){
                ArrayListAnagram.add(possibleWords.get(i));
            }
        }

        return ArrayListAnagram;
    }
}

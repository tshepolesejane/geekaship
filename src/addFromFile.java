/**
 * Created by Tshepo on 10/2/2015.
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class addFromFile {

    public static void main(String[] args) {
        try {

            addNumbers();
        }
        catch (Exception e){
            System.out.println(e);

        }
    }


    public static void addNumbers(){
        try {
            int sum = 0;

            String output = "";
            int row = countRows();
            FileReader fileReader = new FileReader("numberToAdd.txt");
            Scanner scan = new Scanner(fileReader);
            for (int count = 0; count < row; count++) {

                int number = Integer.parseInt(scan.nextLine());
                output = output + number + " + ";

                sum += number;
            }
            String display = output.substring(0, output.length() - 3);
            display = display + " = " + sum;

            System.out.println(display);
        } catch (Exception e){
            System.out.println(e);
        }

    }

    public static int countRows()throws IOException{
        int rows=0;

        FileReader fileReader = new FileReader("numberToAdd.txt");
        BufferedReader br = new BufferedReader(fileReader);

        while (br.readLine()!=null){
            rows++;
        }


        return rows;
    }
}


import java.util.Scanner;

/**
 * Created by Tshepo on 05/9/2015.
 */
public class bruteForceSearch {
    private char[] text;
    private char[] pattern;
    private int lengthOfText;
    private int pattern2;

    public static void main(String[] args) {
        bruteForceSearch bfs = new bruteForceSearch();
        System.out.println("Enter your text to be searched :");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println("Enter your pattern to use in order to search: ");
        String pattern = scanner.nextLine();

        bfs.setString(text, pattern);
        int first_occur_position = bfs.search();
        System.out.println("The text '" + pattern + "' is first found after the " + first_occur_position + " position.");


    }

    public void setString(String text , String pattern) {
        this.text = text.toCharArray();
        this.pattern = pattern.toCharArray();
        this.lengthOfText = text.length();
        this.pattern2= pattern.length();
    }

    public int search() {
        for (int textIndex = 0; textIndex < lengthOfText - pattern2; textIndex++) {
            int patternIndex = 0;
            while (patternIndex < pattern2 && text[textIndex + patternIndex] == pattern[patternIndex]) {
                patternIndex++;
            }
            if (patternIndex == pattern2) return textIndex;
        }
        return -1;
    }


}

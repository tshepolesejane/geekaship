import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Tshepo on 10/8/2015.
 */
public class takingNotes {
    public static void main ( String [] args){

        try {

            File textFile = new File("NOTES.txt");
            Date currentDay = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");
            String todayDate = dateFormat.format(currentDay);

            if (textFile.exists() == true) {
                int argumentLength = args.length;
                writeDate(argumentLength, todayDate, args);
            }else {
                textFile.createNewFile();
            }
        } catch (Exception e){
            System.out.println(e);
        }

        for(String printArgs : args){
            System.out.println(printArgs);
        }

    }
    public static void writeDate(int argsLength, String today, String [] arguments) throws IOException {
        if (argsLength > 0) {
            String printdate = today;
            FileWriter fileW = new FileWriter("NOTES.txt");
            BufferedWriter writeDay = new BufferedWriter(fileW);
            writeDay.write("\n"+ today + "\n");

            for(String printArgs : arguments){
                System.out.println(printArgs);
            }

            writeDay.close();

        }else {
            PrintWriter write = new PrintWriter("NOTES.txt", "UTF-8");
            write.println("Hello world"+ "\n");
            write.println("\n"+ today + "\n");
            write.close();
        }
    }

}

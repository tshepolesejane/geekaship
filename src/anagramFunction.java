import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Tshepo on 10/2/2015.
 */
public class anagramFunction {
    private String [] fileNames;

    public String[] getWordsFromFile() throws Exception {

        readFile();
        return this.fileNames;
    }

    public void readFile()throws Exception{

        URL fielLocation = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
        BufferedReader read = new BufferedReader(new InputStreamReader(fielLocation.openStream()));
        int index =0;
        fileNames = new String[count()];

        String word = read.readLine();

        while (word!=null){
            fileNames[index]=word;
            word = read.readLine();
            index++;

        }

        //fielLocation.Close();

    }

    public int count() throws Exception {
        int count=0;
        URL fileLoc = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fileLoc.openStream()));

        while (br.readLine()!=null){
            count++;
        }

        return count;
    }

    public ArrayList<String> getAnagram(String input, String[] array){
        ArrayList<String> words = new ArrayList<String>();
        String wordAtIndex;
        for (int i = 0; i <array.length ; i++) {

            wordAtIndex = array[i];

            if(wordAtIndex.length()==input.length()){
                words.add(wordAtIndex);
            }
        }

        return  words;
    }

    public boolean isAnAnagram(String input,String posibleWord){
        boolean isAnagram = true;



        for (int j = 0; j <input.length() ; j++) {

            if(!posibleWord.contains(""+input.charAt(j))){

                isAnagram = false;
            }
        }

        return  isAnagram;
    }
}